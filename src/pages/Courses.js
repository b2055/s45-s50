import { Fragment, useEffect, useState } from 'react'
//import courses from '../data/courses'
import CourseCard from '../components/CourseCard'

export default function Courses() {
    //Checks to see if the mock data was captured
    // console.log(courses)
    // console.log(courses[0])

    //State that will be used to store the courses retreived from the database

    const [courses, setCourses] = useState([])

    //retrieve the courses from the database upon initial render of the "Courses" component
    useEffect(() => {
        fetch('http://localhost:4000/api/courses/all')
        .then(res => res.json())
        .then(data => {
            console.log(data)

            // sets the "courses" state to ap the data retrieved from the fetch request into several "CourseCard" components
            setCourses(
                data.map(course => {
                    return (
                        <CourseCard key={course._id} courseProp={course} />
                    )
                }) 
            )
        })
    }, [])

    /* 
        const courseData = courses.map(course => {
            return (
                <CourseCard key={course.id} courseProp={course} />
            )
        }) 
    */
    
    return(
        <Fragment>
            <h1 className="mt-3">Courses</h1>
                {courses}
        </Fragment>
        
    )
}