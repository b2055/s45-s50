import { Redirect } from 'react-router-dom'
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext'


export default function Login() {

    //localStorage.clear()
    
    const {unsetUser, setUser} = useContext(UserContext)

    //clear local storage of the user's information

    unsetUser();

    useEffect(() => {
        // Set the user state back to it's original value

        setUser({id: null})
    })
    return(
        <Redirect to='/login' />
    )
}