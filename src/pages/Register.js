import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){

	const { user } = useContext(UserContext)

    const history = useHistory()

	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [age, setAge] = useState('');
	const [gender, setGender] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');	
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);


	// Enroll function to enroll a user to a specific course



	const registerUser = (e) => {

		e.preventDefault();

		fetch('http://localhost:4000/api/users/checkEmail', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
			
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Duplicate email detected!",
					icon: "error",
					text: "This email address is already registered."
				})
			}
			else{
				fetch('http://localhost:4000/api/users/register', {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						age: age,
						gender: gender,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
					
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);
	
					if(data === true){
						Swal.fire({
							title: "Successfully registered",
							icon: "success",
							text: "You have successfully registered an account."
						})
						setFirstName('');
						setLastName('');
						setAge('');
						setGender('');
						setMobileNo('');
						setEmail('');
						setPassword1('');
						setPassword2('');
						history.push("/login")
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})
	}

	useEffect(() => {
		// Validation to enable the submit buttion when all fields are populated and both passwords match
		if((firstName !== ''
			&& lastName !== ''
			&& age !== ''
			&& gender !== ''
			&& mobileNo !== ''
 			&& email !== '' 
			&& password1 !== '' 
			&& password2 !== '') 
			&& (password1 === password2)
			&& (mobileNo.length === 11)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, age, gender, mobileNo, email, password1, password2])

	return (
		(user.id !== null) ?
		    <Redirect to="/courses" />
		:
		<Container>
			<h1>Register</h1>
			<Form className="mt-3" onSubmit={(e) => registerUser(e)}>

				<Form.Group className="mb-3" controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Enter First Name" 
						value = {firstName}
						onChange = { e => setFirstName(e.target.value)}
						required 
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Enter Last Name" 
						value = {lastName}
						onChange = { e => setLastName(e.target.value)}
						required 
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="age">
					<Form.Label>Age</Form.Label>
					<Form.Control 
						type="Number" 
						placeholder="Enter Age" 
						value = {age}
						onChange = { e => setAge(e.target.value)}
						required 
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="gender">
					<Form.Label>Gender</Form.Label>

					<Form.Select
						type="text" 
						placeholder="Select Gender" 
						value = {gender}
						onChange = { e => setGender(e.target.value)}
						required 
					>
						<option value="">Select Gender</option>
						<option value="Male">Male</option>
						<option value="Female">Female</option>
						<option value="Other">Other</option>
						</Form.Select>
				</Form.Group>

				<Form.Group className="mb-3" controlId="mobileNo">
					<Form.Label>Mobile No.</Form.Label>
					<Form.Control 
						type="Number" 
						placeholder="Enter Mobile No." 
						value = {mobileNo}
						onChange = { e => setMobileNo(e.target.value)}
						required 
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="userEmail">
					<Form.Label>Email address</Form.Label>
					<Form.Control 
						type="email" 
						placeholder="Enter email" 
						value = {email}
						onChange = { e => setEmail(e.target.value)}
						required 
					/>
					<Form.Text className="text-muted">
					We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group className="mb-3" controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Password" 
						value={password1}
						onChange = { e => setPassword1(e.target.value)}
						required 
					/>
				</Form.Group>
				
				<Form.Group className="mb-3" controlId="password2">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Verify Password" 
						value={password2}
						onChange = { e => setPassword2(e.target.value)}
						required 
					/>
				</Form.Group>
				{/* Conditionally render submit button based on isActive state */}
				{ isActive ? 
						<Button variant="primary" type="submit" id="submitBtn">
						Submit
						</Button>
					:
						<Button variant="primary" type="submit" id="submitBtn" disabled>
						Submit
						</Button>
				}
			  
			</Form>
		</Container>
	)
}
