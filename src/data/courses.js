//Mock Database
const courseData = [
    {
        id : "wdc001",
        name : "PHP - Laravel",
        description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet volutpat nunc. Nulla in nunc at lacus faucibus pellentesque vel iaculis nibh. In faucibus nisi sit amet lectus dictum dictum sed vitae ante. Maecenas ornare sodales massa, sed tincidunt mi tempus eu. In ac odio commodo, tincidunt ante eget, feugiat lorem. Maecenas lacinia tellus felis, in sollicitudin turpis auctor a. Nulla facilisi. Etiam id lacinia dolor, vel dictum massa. Ut ornare tristique libero in vehicula. Donec consectetur interdum est, at finibus dui lobortis nec.",
        price : 45000,
        onOffer : true
    },
    {
        id : "wdc002",
        name : "Python - Django",
        description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet volutpat nunc. Nulla in nunc at lacus faucibus pellentesque vel iaculis nibh. In faucibus nisi sit amet lectus dictum dictum sed vitae ante. Maecenas ornare sodales massa, sed tincidunt mi tempus eu. In ac odio commodo, tincidunt ante eget, feugiat lorem. Maecenas lacinia tellus felis, in sollicitudin turpis auctor a. Nulla facilisi. Etiam id lacinia dolor, vel dictum massa. Ut ornare tristique libero in vehicula. Donec consectetur interdum est, at finibus dui lobortis nec.",
        price : 50000,
        onOffer : true
    },
    {
        id : "wdc003",
        name : "Java - Springboot",
        description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet volutpat nunc. Nulla in nunc at lacus faucibus pellentesque vel iaculis nibh. In faucibus nisi sit amet lectus dictum dictum sed vitae ante. Maecenas ornare sodales massa, sed tincidunt mi tempus eu. In ac odio commodo, tincidunt ante eget, feugiat lorem. Maecenas lacinia tellus felis, in sollicitudin turpis auctor a. Nulla facilisi. Etiam id lacinia dolor, vel dictum massa. Ut ornare tristique libero in vehicula. Donec consectetur interdum est, at finibus dui lobortis nec.",
        price : 55000,
        onOffer : true
    },
    {
        id : "wdc004",
        name : "HTML Introduction",
        description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sit amet volutpat nunc. Nulla in nunc at lacus faucibus pellentesque vel iaculis nibh. In faucibus nisi sit amet lectus dictum dictum sed vitae ante. Maecenas ornare sodales massa, sed tincidunt mi tempus eu. In ac odio commodo, tincidunt ante eget, feugiat lorem. Maecenas lacinia tellus felis, in sollicitudin turpis auctor a. Nulla facilisi. Etiam id lacinia dolor, vel dictum massa. Ut ornare tristique libero in vehicula. Donec consectetur interdum est, at finibus dui lobortis nec.",
        price : 60000,
        onOffer : true
    }
]

export default courseData